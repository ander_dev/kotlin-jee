package thinksoft.service

import thinksoft.entities.dto.UserDTO

interface UserService {

    fun findByEmail(email: String) : UserDTO

    fun save(userDTO: UserDTO) : UserDTO

    fun login(username: String, password: String) : UserDTO?

    fun findAll()
}

