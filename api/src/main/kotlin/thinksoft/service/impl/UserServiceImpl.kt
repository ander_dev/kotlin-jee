package thinksoft.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import thinksoft.entities.User
import thinksoft.entities.dto.UserDTO
import thinksoft.repository.RoleRepository
import thinksoft.repository.UserRepository
import thinksoft.service.UserService

@Service("userService")
class UserServiceImpl : UserService {

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var roleRepository: RoleRepository

    override fun findByEmail(email: String): UserDTO = userRepository.findByEmail(email).toDto()

    override fun save(userDTO: UserDTO): UserDTO {
        var roleId:Long
        if (userDTO.id == null){
            userDTO.password = BCryptPasswordEncoder().encode(userDTO.password)
            roleId = if (userDTO.username.equals("ander.dev")){
                1
            } else if (userDTO.superUser == null && userDTO.role == null){
                2
            } else {
                3
            }
            userDTO.role = roleRepository.findOne(roleId)
        }

        var user = User.fromDto(userDTO)

        if(userDTO.superUser != null) user.superUser = userRepository.findOne(userDTO.superUser!!.id)

        return userRepository.save(user).toDto()
    }

    override fun login(username: String, password: String): UserDTO? {
        var user = userRepository.findByUsername(username)

        var pass = BCryptPasswordEncoder().matches(password, user.password)

        if(pass){
            return user.toDto()
        }
        return null
    }

    override fun findAll() {
        userRepository.findAll()
    }
}