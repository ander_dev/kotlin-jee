package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import thinksoft.entities.User

@Repository("userRepository")
interface UserRepository : JpaRepository<User, Long> {

    fun findByEmail(email: String) : User

    fun findByUsername(username: String): User
}