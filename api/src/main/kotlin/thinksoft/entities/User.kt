package thinksoft.entities

import thinksoft.entities.dto.SuperUserDTO
import thinksoft.entities.dto.UserDTO
import java.time.Instant
import javax.persistence.*

@Entity
@Table(name = "user",
        uniqueConstraints = arrayOf(
                UniqueConstraint(name = "unique_username",columnNames = arrayOf("username")),
                UniqueConstraint(name = "unique_email", columnNames = arrayOf("email"))
        ),
        indexes = arrayOf(
                Index(name = "idx_user_username",  columnList="username", unique = true),
                Index(name = "idx_user_email",  columnList="email", unique = true)
        )
)
@NamedQueries(NamedQuery(name = "user.findByUsername", query = "SELECT us FROM User us WHERE us.username = :username"))
data class User(
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long? = null,
        var name: String,
        var email: String,
        var username: String,
        var password: String,
        var oldPassword: String? = null,
        var language: String,
        var gmt: String,
        @Column(columnDefinition="bit default 0")
        var excluded: Boolean = false,
        @Column(columnDefinition="bit default 0")
        var credentialExpired: Boolean = false,
        @Column(columnDefinition="bit default 0")
        var defaultPassword: Boolean = false,
        var registered: Instant? = null,
        @Column(columnDefinition="bit default 0")
        var emailVerified: Boolean = false,
        @Column(columnDefinition="bit default 0")
        var admin: Boolean = false,
        @ManyToOne(targetEntity = Role :: class, fetch = FetchType.EAGER)
        @JoinColumn(name="role_id", foreignKey = ForeignKey(name = "FK_USER_ROLE"))
        var role: Role,
        @ManyToOne(cascade = arrayOf(CascadeType.PERSIST), targetEntity = User::class, fetch = FetchType.EAGER)
        @JoinColumn(name = "super_user_id", foreignKey = ForeignKey(name = "FK_USER_USER"))
        var superUser: User? = null,
        @Transient
        var token: String? = null,
        @Transient
        var confirmPassword: String? = null,
        @Transient
        var authenticationUrl: String? = null
) {
    override fun toString(): String {
        return "User (id=$id, name=$name, username=$username, registered=$registered, password=$password, excluded=$excluded, credentialExpired=$credentialExpired, role=$role)"
    }

    fun toDto(): UserDTO = UserDTO(
            id = this.id!!,
            name = this.name,
            email = this.email,
            username = this.username,
            password = this.password,
            oldPassword = this.oldPassword,
            excluded = this.excluded,
            credentialExpired = this.credentialExpired,
            language = this.language,
            gmt = this.gmt,
            defaultPassword = this.defaultPassword,
            registered = this.registered,
            emailVerified = this.emailVerified,
            role = this.role,
            superUser = this.superUser?.let { SuperUserDTO.toDto(it) },
            admin = this.admin,
            token = this.token,
            confirmPassword = this.confirmPassword,
            authenticationUrl = this.authenticationUrl
    )

    companion object {

        fun fromDto(dto: UserDTO) = User(
                id = dto.id,
                name = dto.name,
                email = dto.email,
                username = dto.username,
                password = dto.password,
                oldPassword = dto.oldPassword,
                excluded =  dto.excluded,
                credentialExpired = dto.credentialExpired,
                language = dto.language,
                gmt = dto.gmt,
                defaultPassword = dto.defaultPassword,
                registered = Instant.now(),
                emailVerified = dto.emailVerified,
                role = dto.role!!,
                admin = dto.admin,
                token = dto.token,
                confirmPassword = dto.confirmPassword,
                authenticationUrl = dto.authenticationUrl)
    }
}
