package thinksoft.entities

import javax.persistence.*

@Entity
@Table(name = "role",
        uniqueConstraints = arrayOf(
                UniqueConstraint(name = "unique_role",columnNames = arrayOf("role"))
        ),
        indexes = arrayOf(
                Index(name = "idx_role_role",  columnList="role", unique = true)
        )
)
data class Role(
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id : Long? = null,
        var role: String? = null,
        var description: String? = null
) {
    override fun toString(): String {
        return "Role (id=$id, role=$role, description=$description)"
    }
}