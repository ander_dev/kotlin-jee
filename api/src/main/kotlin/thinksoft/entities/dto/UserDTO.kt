package thinksoft.entities.dto

import thinksoft.entities.Role
import thinksoft.entities.User
import java.time.Instant

data class UserDTO(
        var id: Long? = null,
        var name: String,
        var email: String,
        var username: String,
        var password: String,
        var oldPassword: String? = null,
        var excluded: Boolean = false,
        var credentialExpired: Boolean = false,
        var language: String,
        var gmt: String,
        var defaultPassword: Boolean = false,
        var registered: Instant? = null,
        var emailVerified: Boolean = false,
        var role: Role? = null,
        var superUser: SuperUserDTO? = null,
        var admin: Boolean = false,
        var token: String? = null,
        var confirmPassword: String? = null,
        var authenticationUrl: String? = null
) {


    companion object {

        fun fromDto(dto: SuperUserDTO) = User(
                id = dto.id,
                name = dto.name!!,
                email = dto.email!!,
                username = dto.username!!,
                password = dto.password!!,
                oldPassword = dto.oldPassword,
                excluded =  dto.excluded,
                credentialExpired = dto.credentialExpired,
                language = dto.language!!,
                gmt = dto.gmt!!,
                defaultPassword = dto.defaultPassword,
                registered = Instant.now(),
                emailVerified = dto.emailVerified,
                role = dto.role!!,
                superUser = dto.superUser!!,
                admin = dto.admin,
                token = dto.token,
                confirmPassword = dto.confirmPassword,
                authenticationUrl = dto.authenticationUrl)
    }
}