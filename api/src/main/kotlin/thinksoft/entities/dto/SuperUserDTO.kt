package thinksoft.entities.dto

import thinksoft.entities.Role
import thinksoft.entities.User
import java.time.Instant

data class SuperUserDTO(
        var id: Long? = null,
        var name: String? = null,
        var email: String? = null,
        var username: String? = null,
        var password: String? = null,
        var oldPassword: String? = null,
        var excluded: Boolean = false,
        var credentialExpired: Boolean = false,
        var language: String? = null,
        var gmt: String? = null,
        var defaultPassword: Boolean = false,
        var registered: Instant? = null,
        var emailVerified: Boolean = false,
        var role: Role? = null,
        var superUser: User? = null,
        var admin: Boolean = false,
        var token: String? = null,
        var confirmPassword: String? = null,
        var authenticationUrl: String? = null
) {
    companion object {
        fun toDto(user: User) = SuperUserDTO(
                id = user.id,
                name = user.name,
                email = user.email,
                username = user.username,
                password = user.password,
                oldPassword = user.oldPassword,
                excluded =  user.excluded,
                credentialExpired = user.credentialExpired,
                language = user.language,
                gmt = user.gmt,
                defaultPassword = user.defaultPassword,
                registered = user.registered,
                emailVerified = user.emailVerified,
                role = user.role,
                superUser = user.superUser,
                admin = user.admin,
                token = user.token,
                confirmPassword = user.confirmPassword,
                authenticationUrl = user.authenticationUrl)
    }
}