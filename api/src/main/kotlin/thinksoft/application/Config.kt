package thinksoft.application

import controller.UserController
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.annotation.EnableTransactionManagement
import thinksoft.entities.Role
import thinksoft.entities.User

@Configuration
@EntityScan(basePackageClasses = arrayOf(User::class, Role::class))
@EnableTransactionManagement
internal class Config {

    @Bean
    fun userController() = UserController()

}