package thinksoft.application

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
class SecurityWebInitializer : WebSecurityConfigurerAdapter(){

    override fun configure(http: HttpSecurity) {
        //This tells Spring Security to authorize all requests
        http.authorizeRequests().anyRequest().authenticated().and().httpBasic()
        http.csrf().disable()
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        //This code sets up a user store in memory. This is
        //useful for debugging and development
        auth
                .inMemoryAuthentication()
                .withUser("Thinksoft")
                .password("@Th1nks0ft")
                .roles("ADMIN")
//                .and()
//                .withUser("Test")
//                .password("test")
//                .roles("USER")
    }
}