package thinksoft.controller

import junit.framework.Assert
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import thinksoft.entities.dto.UserDTO


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class UserControllerUnitTest {


    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    val headers = HttpHeaders()


    @Test
    fun test01CreateSuperUser(){
        var user = "{\"name\": \"Unit Test\",\"username\": \"unit.test\",\"email\": \"unit.test@gmail.com\",\"password\": \"unitpass\",\"language\": \"en\",\"gmt\": \"GMT+13\"}"
        headers.contentType = MediaType.APPLICATION_JSON
        val entity = HttpEntity<String>(user, headers)
        var result = testRestTemplate.withBasicAuth("Thinksoft", "@Th1nks0ft").postForEntity("/rest/user", entity, UserDTO::class.java)
        Assert.assertEquals(result.statusCode, HttpStatus.OK)
        Assert.assertEquals(result.body.role!!.role, "SUPER_USER")
    }

}