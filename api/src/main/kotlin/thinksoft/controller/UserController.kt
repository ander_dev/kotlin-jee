package controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import thinksoft.entities.dto.UserDTO
import thinksoft.service.UserService
import javax.ws.rs.FormParam

@RestController
@RequestMapping(path = arrayOf("/rest"), produces = arrayOf("application/json"))
class UserController {

    @Autowired
    lateinit var userService: UserService

    @GetMapping("/user")
    fun getUsers() = userService.findAll()

    @RequestMapping(value = "/user", method = arrayOf(RequestMethod.POST))
    fun saveUser(@RequestBody userDTO: UserDTO): Any = userService.save(userDTO)

    @GetMapping("/user/{email:.+}")
    fun getUserByEmail(@PathVariable("email") email: String): UserDTO = userService.findByEmail(email)

    @RequestMapping(value = "/user/login", method = arrayOf(RequestMethod.POST))
    fun login(@FormParam("username") username: String, @FormParam("password") password: String): UserDTO? =  userService.login(username, password)

    /** Cause an error to occur */
    @RequestMapping("/raiseError")
    fun raiseError() {
        throw IllegalArgumentException("This shouldn't have happened")
    }

    /** Handle the error */
    @ExceptionHandler(IllegalArgumentException::class)
    @ResponseStatus(HttpStatus.CONFLICT)
    fun handleError(e: IllegalArgumentException) = e.message

}