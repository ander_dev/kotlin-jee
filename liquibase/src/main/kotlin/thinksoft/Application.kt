package thinksoft

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.support.SpringBootServletInitializer

@SpringBootApplication
class Application : SpringBootServletInitializer() {

    /**
     * Run the application
     * @param args The command line arguments
     */
    fun main(args: Array<String>) {
        SpringApplication.run(Application::class.java, *args)
    }

}